#!/usr/bin/env python
#
# zabbix-speedtest - Julius Roberts 2019
#
# a quick interface between zabbix and speedtest-cli
# based on API from https://github.com/sivel/speedtest-cli/wiki
"""
Usage:
  zabbix-speedtest upload
  zabbix-speedtest download
  zabbix-speedtest ping
"""
import speedtest
from docopt import docopt

args = docopt(__doc__, version='0.0.1')

def main():
    # determine best server with servers = []
    # or set a target specific server by id
    servers = [10612]
    s = speedtest.Speedtest()
    s.get_servers(servers)
    s.get_best_server()

    if args['download']:
        s.download()
        download_mbps = s.results.download / 1000000
        print("%.2f" % download_mbps)

    if args['upload']:
        s.upload()
        upload_mbps = s.results.upload / 1000000
        print("%.2f" % upload_mbps)

    if args['ping']:
        print(s.results.ping)

if __name__ == "__main__":
    main()
