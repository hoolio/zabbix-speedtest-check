# zabbix-speedtest-check

A custom zabbix check interfacing zabbix with speedtest-cli.

# requirements on zabbix-agent
1. python 2.7 (but should work with 3)
1. python module docopt
1. python module speedtest-cli

## installation & setup on debian 9
1. system python is fine.
1. sudo apt-get install python-docopt
1. sudo apt-get install speedtest-cli (which is written in python and supplies the speedtest API)
1. git clone this repo (to ~ for example)
1. change the download server id as appropriate

## elsewhere
should work on python 3 and assuming requirements installed via pip etc

# usage and testing
each command will run one check and return a single output as required by zabbix
```
~/zabbix-speedtest-check$ ./zabbix-speedtest.py
Usage:
  zabbix-speedtest upload
  zabbix-speedtest download
  zabbix-speedtest ping
```
## examples
```
~/zabbix-speedtest-check$ ./zabbix-speedtest.py download
41.56
~/zabbix-speedtest-check$ ./zabbix-speedtest.py upload
17.71
~/zabbix-speedtest-check$ ./zabbix-speedtest.py ping
16.25
```

# zabbix-agent config
todo

# integration with zabbix webui
todo
